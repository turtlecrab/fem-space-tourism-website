# Frontend Mentor - Space tourism website

![Preview for the Space Tourism coding challenge](./public/space-screen.gif)

## Links

- Live site: <https://fem-space-tourism-website.vercel.app/>
- Solution page: <https://www.frontendmentor.io/solutions/space-tourism-website-w-react-router-framer-motion-Wp9FuDsWyv>

