import React from 'react'
import ReactDOM from 'react-dom/client'
import { BrowserRouter } from 'react-router-dom'
import { ThemeProvider } from 'styled-components'

import './index.scss'
import App from './App'
import { DataContext } from './DataContext'
import data from './data.json'

const theme = {
  MD: 'screen and (min-width: 620px)',
  LG: 'screen and (min-width: 1200px)',
}

const root = ReactDOM.createRoot(document.getElementById('root') as HTMLElement)
root.render(
  <React.StrictMode>
    <ThemeProvider theme={theme}>
      <DataContext.Provider value={data}>
        <BrowserRouter>
          <App />
        </BrowserRouter>
      </DataContext.Provider>
    </ThemeProvider>
  </React.StrictMode>
)
