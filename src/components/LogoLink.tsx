import { Link } from 'react-router-dom'
import styled from 'styled-components'

import logo from '../assets/logo.svg'

function LogoLink() {
  return (
    <Container>
      <HomeLink to="/">
        <Logo src={logo} alt="Space Tourism logo" />
      </HomeLink>
    </Container>
  )
}

const Container = styled.div`
  flex: 1;
  position: relative;

  @media ${p => p.theme.MD} {
    padding: 24px 39px;
  }

  @media ${p => p.theme.LG} {
    padding: 24px 55px;

    &::after {
      content: '';
      position: absolute;
      background-color: #fff4;
      height: 1px;
      top: 48px;
      left: 167px;
      right: -30px;
      z-index: 20;
    }
  }
`

const HomeLink = styled(Link)`
  display: flex;
  width: 48px;
`

const Logo = styled.img`
  width: 40px;
  height: 40px;

  @media ${p => p.theme.MD} {
    width: 48px;
    height: 48px;
  }
`

export default LogoLink
