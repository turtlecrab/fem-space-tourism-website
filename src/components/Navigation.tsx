import { useState } from 'react'
import { NavLink, useLocation } from 'react-router-dom'
import styled, { useTheme } from 'styled-components'
import { Drawer } from '@mantine/core'
import { motion } from 'framer-motion'

import burgerIcon from '../assets/icon-hamburger.svg'
import closeIcon from '../assets/icon-close.svg'
import useMedia from '../hooks/useMedia'

function Navigation() {
  const [opened, setOpened] = useState(false)

  const close = () => setOpened(false)

  const theme: any = useTheme()
  const isMdUp = useMedia(theme.MD)

  const curPage = useLocation().pathname.split('/')[1]

  const navigation = (
    <Nav>
      {!isMdUp && <CloseButton onClick={close} aria-label="Close navigation" />}
      <List>
        {/* TODO: dry this stuff */}
        <li>
          <MenuLink to="/" onClick={close}>
            <span aria-hidden>00</span>
            Home
            <div className="hoverMarker" />
            {curPage === '' && <ActiveMarker />}
          </MenuLink>
        </li>
        <li>
          <MenuLink to="/destination" onClick={close}>
            <span aria-hidden>01</span>
            Destination
            <div className="hoverMarker" />
            {curPage === 'destination' && <ActiveMarker />}
          </MenuLink>
        </li>
        <li>
          <MenuLink to="/crew" onClick={close}>
            <span aria-hidden>02</span>
            Crew
            <div className="hoverMarker" />
            {curPage === 'crew' && <ActiveMarker />}
          </MenuLink>
        </li>
        <li>
          <MenuLink to="/technology" onClick={close}>
            <span aria-hidden>03</span>
            Technology
            <div className="hoverMarker" />
            {curPage === 'technology' && <ActiveMarker />}
          </MenuLink>
        </li>
      </List>
    </Nav>
  )

  return isMdUp ? (
    navigation
  ) : (
    <>
      <Burger
        onClick={() => setOpened(true)}
        aria-label="Open navigation"
        aria-expanded={opened}
      ></Burger>
      <Drawer
        opened={opened}
        onClose={close}
        position="right"
        size={254}
        overlayOpacity={0}
        withCloseButton={false}
      >
        {navigation}
      </Drawer>
    </>
  )
}

const Nav = styled.nav`
  display: flex;
  flex-direction: column;

  @media ${p => p.theme.MD} {
    background: rgba(255, 255, 255, 0.04);
    backdrop-filter: blur(40px);
    padding: 0 48px;
  }

  @media ${p => p.theme.LG} {
    padding-left: 123px;
    padding-right: 165px;
  }
`

const Burger = styled.button`
  cursor: pointer;
  width: 24px;
  height: 21px;
  border: none;
  background-color: transparent;
  background-image: url(${burgerIcon});
`

const CloseButton = styled.button`
  cursor: pointer;
  align-self: flex-end;
  margin: 33px 24px 49px;
  width: 20px;
  height: 20px;
  display: block;
  border: none;
  background: url(${closeIcon});
`

const List = styled.ul`
  padding: 0;
  margin: 0;
  list-style: none;
  display: flex;
  flex-direction: column;

  @media ${p => p.theme.MD} {
    flex-direction: row;
    gap: 39px;
  }

  @media ${p => p.theme.LG} {
    gap: 48px;
  }
`

const MenuLink = styled(NavLink)`
  font-family: var(--ff-sans);
  font-size: 1rem;
  font-weight: 400;
  letter-spacing: 2.7px;
  color: var(--color-white);
  text-transform: uppercase;
  text-decoration: none;
  margin: 11px 0 11px 32px;
  padding: 5px 0;
  display: block;
  position: relative;

  span {
    padding-right: 12px;
    font-weight: 700;
  }

  @media ${p => p.theme.MD} {
    margin: 0;
    padding: 38px 0 35px;
    font-size: 0.875rem;
    letter-spacing: 2.4px;

    span {
      display: none;
    }

    .hoverMarker {
      position: absolute;
      background-color: transparent;
      width: 4px;
      top: 0;
      right: 0;
      bottom: 0;

      @media ${p => p.theme.MD} {
        width: auto;
        top: auto;
        height: 3px;
        left: 0;
      }
    }

    &:hover {
      .hoverMarker {
        background-color: #fff8;
      }
    }
  }

  @media ${p => p.theme.LG} {
    font-size: 1rem;
    letter-spacing: 2.7px;

    span {
      padding-right: 9px;
      display: inline;
    }
  }
`

const ActiveMarker = styled(motion.div).attrs({ layoutId: 'active' })`
  position: absolute;
  background-color: var(--color-white);
  width: 4px;
  top: 0;
  right: 0;
  bottom: 0;

  @media ${p => p.theme.MD} {
    width: auto;
    top: auto;
    height: 3px;
    left: 0;
  }
`

export default Navigation
