import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { motion } from 'framer-motion'

function Home() {
  return (
    <Container>
      <Sub>So, you want to travel to</Sub>
      <Heading>Space</Heading>
      <Text>
        Let’s face it; if you want to go to space, you might as well genuinely
        go to outer space and not hover kind of on the edge of it. Well sit
        back, and relax because we’ll give you a truly out of this world
        experience!
      </Text>
      <BigLink to="/destination">Explore</BigLink>
    </Container>
  )
}

const Container = styled(motion.div).attrs({
  initial: 'hidden',
  animate: 'show',
  exit: 'exit',
})`
  padding: 24px;
  flex: 1;
  display: grid;
  height: 100%;
  gap: 20px;
  grid-template-rows: auto auto auto 1fr;
  grid-template-columns: 1fr;
  overflow: hidden;

  @media ${p => p.theme.MD} {
    gap: 24px;
    padding-top: 106px;
  }

  @media ${p => p.theme.LG} {
    grid-template-columns: 28.125rem auto;
    grid-template-areas:
      'sub link'
      'header link'
      'text link';
    padding: 251px 165px 108px;
    justify-self: end;
  }
`

const Sub = styled(motion.h1).attrs({
  variants: {
    hidden: { opacity: 0, y: 100 },
    show: { opacity: 1, y: 0, transition: { duration: 0.8 } },
    exit: { opacity: 0, y: 100, transition: { duration: 0.5 } },
  },
})`
  padding: 0;
  margin: auto;
  color: var(--color-light);
  font-family: var(--ff-sans);
  font-size: 1rem;
  letter-spacing: 2.7px;
  text-transform: uppercase;
  font-weight: 400;

  @media ${p => p.theme.MD} {
    font-size: 1.25rem;
    letter-spacing: 3.4px;
  }

  @media ${p => p.theme.LG} {
    font-size: 1.75rem;
    letter-spacing: 4.7px;
    grid-area: sub;
    margin-left: 0;
  }
`

const Heading = styled(motion.p).attrs({
  variants: {
    hidden: { opacity: 0, y: 100 },
    show: { opacity: 1, y: 0, transition: { duration: 0.8 } },
    exit: { opacity: 0, y: 100, transition: { duration: 0.5 } },
  },
})`
  padding: 0;
  margin: auto;
  display: block;
  color: var(--color-white);
  font-family: var(--ff-serif);
  font-size: 5rem;
  line-height: 6.25rem;
  letter-spacing: 0;
  text-transform: uppercase;
  font-weight: 400;

  @media ${p => p.theme.MD} {
    font-size: 9.375rem;
    line-height: 9.375rem;
  }

  @media ${p => p.theme.LG} {
    grid-area: header;
    margin-left: 0;
    margin-top: 12px;
  }
`
const Text = styled(motion.p).attrs({
  variants: {
    hidden: { opacity: 0, y: 100 },
    show: { opacity: 1, y: 0, transition: { duration: 0.8 } },
    exit: { opacity: 0, y: 100, transition: { duration: 0.5 } },
  },
})`
  padding: 0;
  margin: 0 auto 24px;
  max-width: 27.75rem;
  text-align: center;
  color: var(--color-light);
  font-family: var(--ff-main);
  font-size: 0.9375rem;
  line-height: 167%;
  font-weight: 400;

  @media ${p => p.theme.MD} {
    font-size: 1rem;
    margin-bottom: 90px;
  }

  @media ${p => p.theme.LG} {
    font-size: 1.125rem;
    line-height: 178%;
    grid-area: text;
    margin-left: 0;
    text-align: start;
    margin-bottom: 0;
    margin-top: 10px;
  }
`

const BigLink = styled(motion(Link)).attrs({
  variants: {
    hidden: { opacity: 0, y: 100 },
    show: { opacity: 1, y: 0, transition: { duration: 0.8 } },
    exit: { opacity: 0, y: 100, transition: { duration: 0.5 } },
  },
})`
  margin: auto auto 24px;
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  color: var(--bg-dark);
  background-color: var(--color-white);
  font-family: var(--ff-serif);
  font-size: 1.25rem;
  letter-spacing: 1.25px;
  font-weight: 400;
  text-transform: uppercase;
  text-decoration: none;
  width: 7.5em;
  height: 7.5em;
  border-radius: 50%;

  &::before {
    content: '';
    position: absolute;
    z-index: -1;
    background-color: #fff2;
    width: 100%;
    height: 100%;
    border-radius: 50%;
    opacity: 0;
    transition: 0.3s;
  }

  &:hover::before {
    transform: scale(165%);
    opacity: 1;
  }

  @media ${p => p.theme.MD} {
    font-size: 2rem;
    margin-bottom: 66px;
  }

  @media ${p => p.theme.LG} {
    grid-area: link;
    margin: auto 0 0 auto;
    font-size: 2rem;
    letter-spacing: 0.0625em;
    width: 8.5em;
    height: 8.5em;
  }
`

export default Home
