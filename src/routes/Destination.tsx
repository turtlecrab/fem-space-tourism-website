import { motion } from 'framer-motion'
import { useContext, useEffect } from 'react'
import { NavLink, Outlet, useLocation } from 'react-router-dom'
import styled from 'styled-components'

import { DataContext } from '../DataContext'

function Destination() {
  const data = useContext(DataContext)
  const location = useLocation()

  useEffect(() => {
    window.document.body.classList.add('destination')
    return () => window.document.body.classList.remove('destination')
  }, [])

  return (
    <Container initial="hidden" animate="show" exit="exit">
      <Title>
        <span aria-hidden>01</span> Pick your destination
      </Title>
      <List>
        {data.destinations.map(dest => (
          <li key={dest.name}>
            <TabLink to={dest.name.toLowerCase()}>{dest.name}</TabLink>
          </li>
        ))}
      </List>
      {/* TODO: fix it so exit animation plays properly on page change*/}
      <Outlet key={location.pathname} />
    </Container>
  )
}

const Container = styled(motion.div)`
  padding: 0 24px;
  display: grid;
  justify-items: center;

  @media ${p => p.theme.MD} {
    padding: 40px 39px 0;
  }

  @media ${p => p.theme.LG} {
    padding: 76px min(11.45vw, 165px);
    grid-template-areas:
      'title title'
      'img list'
      'img article';
    grid-template-columns: 1fr auto;
    grid-column-gap: 100px;
  }
`

const Title = styled(motion.h1).attrs({
  variants: {
    hidden: { opacity: 0, x: -100 },
    show: { opacity: 1, x: 0, transition: { duration: 0.5 } },
    exit: { opacity: 0, x: -100, transition: { duration: 0.3 } },
  },
})`
  padding: 0;
  margin: 0;
  color: var(--color-white);
  font-family: var(--ff-sans);
  font-weight: 400;
  font-size: 1rem;
  letter-spacing: 2.7px;
  text-transform: uppercase;
  order: 1;

  @media ${p => p.theme.MD} {
    font-size: 1.25rem;
    letter-spacing: 3.38px;
    justify-self: start;
  }

  @media ${p => p.theme.LG} {
    grid-area: title;
    font-size: 1.75rem;
    letter-spacing: 4.72px;
  }

  & > span {
    font-weight: 700;
    opacity: 0.25;
    padding-right: 10px;

    @media ${p => p.theme.MD} {
      padding-right: 5px;
    }

    @media ${p => p.theme.LG} {
      padding-right: 13px;
    }
  }
`

const List = styled(motion.ul).attrs({
  variants: {
    hidden: { opacity: 0, x: 100 },
    show: { opacity: 1, x: 0, transition: { duration: 0.5 } },
    exit: { opacity: 0, x: 100, transition: { duration: 0.3 } },
  },
})`
  padding: 0;
  margin: 14px 0 0;
  list-style: none;
  display: flex;
  gap: 27px;
  order: 3;

  @media ${p => p.theme.MD} {
    margin-top: 38px;
    gap: 36px;
  }

  @media ${p => p.theme.LG} {
    grid-area: list;
    justify-self: start;
    gap: 32px;
    margin-top: 49px;
  }
`

const TabLink = styled(NavLink)`
  position: relative;
  color: var(--color-light);
  font-family: var(--ff-sans);
  font-size: 0.875rem;
  letter-spacing: 2.35px;
  text-transform: uppercase;
  text-decoration: none;
  padding: 12px 0;
  display: block;

  &:hover::before {
    content: '';
    position: absolute;
    left: 0;
    right: 0;
    bottom: 0;
    height: 3px;
    background-color: #fff8;
  }

  &.active {
    color: var(--color-white);

    &::after {
      content: '';
      position: absolute;
      left: 0;
      right: 0;
      bottom: 0;
      height: 3px;
      background-color: var(--color-white);
    }
  }

  @media ${p => p.theme.MD} {
    font-size: 1em;
    letter-spacing: 2.7px;
    padding: 15px 0;
  }
`

export default Destination
