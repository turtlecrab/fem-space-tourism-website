import { motion } from 'framer-motion'
import { useContext } from 'react'
import { Navigate, useParams } from 'react-router-dom'
import styled from 'styled-components'

import { DataContext } from '../DataContext'

function DestinationItem() {
  const data = useContext(DataContext)

  const { dest } = useParams()
  const destData = data.destinations.find(d => d.name.toLowerCase() === dest)

  if (!destData) return <Navigate to=".." />

  return (
    <>
      {/* TODO: use webp here? */}
      <Image src={'.' + destData.images.png} alt={destData.name} />
      <Article>
        <Heading>{destData.name}</Heading>
        <Text>{destData.description}</Text>
        <Line />
        <List>
          <li>
            <InfoHeading>Avg. Distance</InfoHeading>
            <Info>{destData.distance}</Info>
          </li>
          <li>
            <InfoHeading>Est. Travel Time</InfoHeading>
            <Info>{destData.travel}</Info>
          </li>
        </List>
      </Article>
    </>
  )
}

const Image = styled(motion.img).attrs({
  variants: {
    hidden: { opacity: 0, x: -100 },
    show: { opacity: 1, x: 0, transition: { duration: 0.5 } },
    exit: { opacity: 0, x: -100, transition: { duration: 0.3 } },
  },
})`
  margin: 32px 0 0;
  width: 170px;
  aspect-ratio: 1;
  order: 2;

  @media ${p => p.theme.MD} {
    margin-top: 60px;
    width: 300px;
  }

  @media ${p => p.theme.LG} {
    grid-area: img;
    margin-top: 100px;
    width: min(31vw, 445px);
  }
`

const Article = styled(motion.article).attrs({
  variants: {
    hidden: { opacity: 0, x: 100 },
    show: { opacity: 1, x: 0, transition: { duration: 0.5 } },
    exit: { opacity: 0, x: 100, transition: { duration: 0.3 } },
  },
})`
  margin: 20px 0 0;
  display: flex;
  flex-direction: column;
  align-items: center;
  order: 4;
  max-width: 35.8rem;

  @media ${p => p.theme.MD} {
    margin-top: 32px;
  }

  @media ${p => p.theme.LG} {
    grid-area: article;
    width: min(31vw, 445px);
    align-items: start;
    margin-top: 30px;
  }
`

const Heading = styled.h2`
  font-size: 3.5rem;
  font-family: var(--ff-serif);
  font-weight: 400;
  color: var(--color-white);
  text-transform: uppercase;
  padding: 0;
  margin: 0;

  @media ${p => p.theme.MD} {
    font-size: 5rem;
  }

  @media ${p => p.theme.LG} {
    font-size: 6.25rem;
  }
`

const Text = styled.p`
  font-size: 0.9375rem;
  font-family: var(--ff-main);
  font-weight: 400;
  color: var(--color-light);
  padding: 0;
  margin: 0;
  text-align: center;
  line-height: 167%;

  @media ${p => p.theme.MD} {
    margin-top: 8px;
    font-size: 1rem;
    line-height: 175%;
  }

  @media ${p => p.theme.LG} {
    text-align: left;
  }

  @media ${p => p.theme.LG} {
    font-size: 1.125rem;
    line-height: 178%;
    margin-top: 14px;
  }
`

const Line = styled.hr`
  margin: 32px 0 0;
  width: 100%;
  border: 0;
  background-color: var(--color-hr);
  height: 1px;

  @media ${p => p.theme.MD} {
    margin-top: 49px;
  }

  @media ${p => p.theme.MD} {
    margin-top: 54px;
  }
`

const List = styled.ul`
  padding: 0;
  margin: 0 0 32px;
  list-style: none;
  display: flex;
  flex-direction: column;
  align-items: center;
  align-self: stretch;

  @media ${p => p.theme.MD} {
    flex-direction: row;

    & > li {
      flex: 1;
    }
  }

  @media ${p => p.theme.LG} {
    margin: 0;
  }
`

const InfoHeading = styled.h3`
  padding: 0;
  margin: 32px 0 12px;
  color: var(--color-light);
  font-family: var(--ff-sans);
  font-weight: 400;
  font-size: 0.875rem;
  letter-spacing: 2.35px;
  text-transform: uppercase;
  text-align: center;

  @media ${p => p.theme.MD} {
    margin-top: 28px;
  }

  @media ${p => p.theme.LG} {
    text-align: left;
  }
`

const Info = styled.p`
  padding: 0;
  margin: 0;
  color: var(--color-white);
  font-family: var(--ff-serif);
  font-weight: 400;
  font-size: 1.75rem;
  text-transform: uppercase;
  text-align: center;

  @media ${p => p.theme.LG} {
    text-align: left;
  }
`

export default DestinationItem
