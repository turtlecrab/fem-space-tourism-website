import { Link } from 'react-router-dom'
import styled from 'styled-components'

function NotFound() {
  return (
    <Container>
      <h1>404</h1>
      <p>👽👾</p>
      <HomeLink to="/">Return to home page</HomeLink>
    </Container>
  )
}

const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  h1 {
    color: var(--color-white);
    font-size: 5rem;
    font-family: var(--ff-sans);
    font-weight: 400;
    text-transform: uppercase;
    padding: 0;
    margin: 100px 0 16px;
  }

  p {
    text-transform: uppercase;
    color: var(--color-light);
    font-size: 2rem;
    font-family: var(--ff-sans);
    font-weight: 400;
    color: var(--color-light);
    letter-spacing: 2.7px;
    text-transform: uppercase;
    padding: 0;
    margin: 0;
  }
`

const HomeLink = styled(Link)`
  font-family: var(--ff-sans);
  font-size: 1.125rem;
  font-weight: 400;
  letter-spacing: 2.7px;
  color: var(--color-white);
  text-transform: uppercase;
  text-decoration: none;
  margin: 50px 0 0;
  background: rgba(255, 255, 255, 0.04);
  backdrop-filter: blur(40px);
  padding: 24px;
  border-radius: 24px;
`

export default NotFound
