import { motion } from 'framer-motion'
import { useContext, useEffect } from 'react'
import { NavLink, Outlet, useLocation } from 'react-router-dom'
import styled from 'styled-components'

import { DataContext } from '../DataContext'

function Crew() {
  const data = useContext(DataContext)
  const location = useLocation()

  useEffect(() => {
    window.document.body.classList.add('crew')
    return () => window.document.body.classList.remove('crew')
  }, [])

  return (
    <Container>
      <Title>
        <span aria-hidden>02</span> Meet your crew
      </Title>
      <List>
        {data.crew.map(crew => (
          <li key={crew.role}>
            <CircleTabLink to={crew.role.toLowerCase().replace(/ /g, '-')}>
              <span>{crew.role}</span>
              {/* extra div to make padding area clickable/touchable */}
              <div></div>
            </CircleTabLink>
          </li>
        ))}
      </List>
      <Outlet key={location.pathname} />
    </Container>
  )
}

const Container = styled(motion.div).attrs({
  initial: 'hidden',
  animate: 'show',
  exit: 'exit',
})`
  padding: 0 24px;
  display: grid;
  justify-items: center;

  @media ${p => p.theme.MD} {
    height: 100%;
    grid-template-rows: auto auto auto 1fr;
    padding: 40px 39px 0;
  }

  @media ${p => p.theme.LG} {
    padding: 76px min(11.45vw, 165px) 0;
    grid-template-areas:
      'title img'
      'article img'
      'list img';
    grid-template-columns: 1fr auto;
    grid-template-rows: auto 1fr auto;
    grid-column-gap: 100px;
    justify-self: end;
  }
`

const Title = styled(motion.h1).attrs({
  variants: {
    hidden: { opacity: 0, x: -100 },
    show: { opacity: 1, x: 0, transition: { duration: 0.5 } },
    exit: { opacity: 0, x: -100, transition: { duration: 0.3 } },
  },
})`
  padding: 0;
  margin: 0;
  color: var(--color-white);
  font-family: var(--ff-sans);
  font-weight: 400;
  font-size: 1rem;
  letter-spacing: 2.7px;
  text-transform: uppercase;
  order: 1;

  @media ${p => p.theme.MD} {
    font-size: 1.25rem;
    letter-spacing: 3.38px;
    justify-self: start;
  }

  @media ${p => p.theme.LG} {
    grid-area: title;
    font-size: 1.75rem;
    letter-spacing: 4.72px;
  }

  & > span {
    font-weight: 700;
    opacity: 0.25;
    padding-right: 10px;

    @media ${p => p.theme.MD} {
      padding-right: 5px;
    }

    @media ${p => p.theme.LG} {
      padding-right: 13px;
    }
  }
`

const List = styled(motion.ul).attrs({
  variants: {
    hidden: { opacity: 0, x: -100 },
    show: { opacity: 1, x: 0, transition: { duration: 0.5 } },
    exit: { opacity: 0, x: -100, transition: { duration: 0.3 } },
  },
})`
  padding: 0;
  margin: 24px 0 0;
  list-style: none;
  display: flex;
  order: 3;

  @media ${p => p.theme.LG} {
    grid-area: list;
    justify-self: start;
    margin: 96px 0;
  }
`

const CircleTabLink = styled(NavLink)`
  display: block;
  margin: 0;
  padding: 8px;

  &:hover {
    --opacity: 0.5;
  }

  &.active {
    --opacity: 1;
  }

  & > div {
    width: 0.625rem;
    height: 0.625rem;
    border-radius: 50%;
    background-color: var(--color-white);
    opacity: var(--opacity, 0.174);

    @media ${p => p.theme.LG} {
      width: 0.9375rem;
      height: 0.9375rem;
    }
  }

  & > span {
    position: absolute;
    width: 1px;
    height: 1px;
    padding: 0;
    margin: -1px;
    overflow: hidden;
    clip: rect(0, 0, 0, 0);
    white-space: nowrap;
    border-width: 0;
  }

  @media ${p => p.theme.LG} {
    padding: 12px;
  }
`

export default Crew
