import { motion } from 'framer-motion'
import { useContext, useEffect } from 'react'
import { NavLink, Outlet, useLocation } from 'react-router-dom'
import styled from 'styled-components'

import { DataContext } from '../DataContext'

function Technology() {
  const data = useContext(DataContext)
  const location = useLocation()

  useEffect(() => {
    window.document.body.classList.add('technology')
    return () => window.document.body.classList.remove('technology')
  }, [])

  return (
    <Container>
      <Title>
        <span aria-hidden>03</span> Space launch 101
      </Title>
      <List>
        {data.technology.map((tech, index) => (
          <li key={tech.name}>
            <NumberTabLink to={tech.name.toLowerCase().replace(/ /g, '-')}>
              <span aria-hidden={true}>{index + 1}</span>
              <SrOnly>{tech.name}</SrOnly>
            </NumberTabLink>
          </li>
        ))}
      </List>
      <Outlet key={location.pathname} />
    </Container>
  )
}

const Container = styled(motion.div).attrs({
  initial: 'hidden',
  animate: 'show',
  exit: 'exit',
})`
  padding: 0;
  display: grid;
  justify-items: center;

  @media ${p => p.theme.MD} {
    padding: 40px 0 0;
  }

  @media ${p => p.theme.LG} {
    padding: 76px 0 100px min(11.45vw, 165px);
    grid-template-areas:
      'title title title'
      'list article img';
    grid-template-columns: auto 1fr 1fr;
    align-items: center;
  }
`

const Title = styled(motion.h1).attrs({
  variants: {
    hidden: { opacity: 0, x: -100 },
    show: { opacity: 1, x: 0, transition: { duration: 0.5 } },
    exit: { opacity: 0, x: -100, transition: { duration: 0.3 } },
  },
})`
  padding: 0;
  margin: 0;
  color: var(--color-white);
  font-family: var(--ff-sans);
  font-weight: 400;
  font-size: 1rem;
  letter-spacing: 2.7px;
  text-transform: uppercase;
  order: 1;

  @media ${p => p.theme.MD} {
    font-size: 1.25rem;
    letter-spacing: 3.38px;
    justify-self: start;
    padding-left: 39px;
  }

  @media ${p => p.theme.LG} {
    grid-area: title;
    font-size: 1.75rem;
    letter-spacing: 4.72px;
    padding: 0;
  }

  & > span {
    font-weight: 700;
    opacity: 0.25;
    padding-right: 10px;

    @media ${p => p.theme.MD} {
      padding-right: 5px;
    }

    @media ${p => p.theme.LG} {
      padding-right: 13px;
    }
  }
`

const List = styled(motion.ul).attrs({
  variants: {
    hidden: { opacity: 0, x: -100 },
    show: { opacity: 1, x: 0, transition: { duration: 0.5 } },
    exit: { opacity: 0, x: -100, transition: { duration: 0.3 } },
  },
})`
  padding: 0;
  margin: 34px 0 0;
  list-style: none;
  display: flex;
  gap: 16px;
  order: 3;

  @media ${p => p.theme.MD} {
    margin-top: 56px;
  }

  @media ${p => p.theme.LG} {
    grid-area: list;
    flex-direction: column;
    gap: 32px;
    margin: 124px 0 100px;
  }
`

const NumberTabLink = styled(NavLink)`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 2.5em;
  height: 2.5em;
  border-radius: 50%;
  border: 1px solid #fff4;
  padding: 0;
  margin: 0;
  color: var(--color-white);
  font-family: var(--ff-serif);
  font-weight: 400;
  font-size: 1rem;
  text-decoration: none;

  &:hover {
    border-color: var(--color-white);
  }

  &.active {
    background-color: var(--color-white);
    border-color: var(--color-white);
    color: black;
  }

  @media ${p => p.theme.MD} {
    font-size: 1.5rem;
  }

  @media ${p => p.theme.LG} {
    font-size: 2rem;
  }
`

const SrOnly = styled.span`
  position: absolute;
  width: 1px;
  height: 1px;
  padding: 0;
  margin: -1px;
  overflow: hidden;
  clip: rect(0, 0, 0, 0);
  white-space: nowrap;
  border-width: 0;
`

export default Technology
