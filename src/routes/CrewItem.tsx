import { motion } from 'framer-motion'
import { useContext } from 'react'
import { Navigate, useParams } from 'react-router-dom'
import styled from 'styled-components'

import { DataContext } from '../DataContext'

function CrewItem() {
  const data = useContext(DataContext)

  const role = useParams().role?.replace(/-/g, ' ')
  const crewData = data.crew.find(d => d.role.toLowerCase() === role)

  if (!crewData) return <Navigate to=".." />

  return (
    <>
      <ImageWrapper>
        <Image src={'.' + crewData.images.png} alt={crewData.name} />
      </ImageWrapper>
      <Article>
        <Role>{crewData.role}</Role>
        <Name>{crewData.name}</Name>
        <Text>{crewData.bio}</Text>
      </Article>
    </>
  )
}

const ImageWrapper = styled(motion.div).attrs({
  variants: {
    hidden: { opacity: 0, x: 100 },
    show: { opacity: 1, x: 0, transition: { duration: 0.5 } },
    exit: { opacity: 0, x: 100, transition: { duration: 0.3 } },
  },
})`
  width: 100%;
  padding: 0;
  margin: 0;
  /* TODO: fix 1px gap on chrome */
  border-bottom: 1px solid var(--color-hr);
  display: flex;
  flex-direction: column;
  align-items: center;
  order: 2;

  @media ${p => p.theme.MD} {
    order: 4;
    justify-content: end;
  }

  @media ${p => p.theme.LG} {
    grid-area: img;
  }
`

const Image = styled.img`
  margin: 32px 0 0;
  width: 170px;
  border: none;

  @media ${p => p.theme.MD} {
    height: 532px;
    width: auto;
  }

  @media ${p => p.theme.LG} {
    height: auto;
  }
`

const Article = styled(motion.article).attrs({
  variants: {
    hidden: { opacity: 0, x: -100 },
    show: { opacity: 1, x: 0, transition: { duration: 0.5 } },
    exit: { opacity: 0, x: -100, transition: { duration: 0.3 } },
  },
})`
  margin: 24px 0 32px;
  display: flex;
  flex-direction: column;
  align-items: center;
  order: 4;

  @media ${p => p.theme.MD} {
    margin: 60px 0 9px;
    order: 2;
  }

  @media ${p => p.theme.LG} {
    grid-area: article;
    justify-self: start;
    align-items: flex-start;
    margin-top: 154px;
  }
`

const Role = styled.h2`
  font-size: 1rem;
  font-family: var(--ff-serif);
  font-weight: 400;
  color: #fff8;
  text-transform: uppercase;
  padding: 0;
  margin: 0;

  @media ${p => p.theme.MD} {
    font-size: 1.5rem;
  }

  @media ${p => p.theme.LG} {
    font-size: 2rem;
  }
`

const Name = styled.p`
  font-size: 1.5rem;
  font-family: var(--ff-serif);
  font-weight: 400;
  color: var(--color-white);
  text-transform: uppercase;
  padding: 0;
  margin: 8px 0 16px;

  @media ${p => p.theme.MD} {
    font-size: 2.5rem;
  }

  @media ${p => p.theme.LG} {
    font-size: 3.5rem;
    margin: 15px 0 27px;
  }
`

const Text = styled.p`
  font-size: 0.9375rem;
  font-family: var(--ff-main);
  font-weight: 400;
  color: var(--color-light);
  padding: 0;
  margin: 0;
  text-align: center;
  line-height: 167%;

  @media ${p => p.theme.MD} {
    font-size: 1rem;
    line-height: 175%;
    width: 28.625rem;
  }

  @media ${p => p.theme.LG} {
    text-align: left;
    font-size: 1.125rem;
    line-height: 178%;
  }
`
export default CrewItem
