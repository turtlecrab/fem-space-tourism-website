import { motion } from 'framer-motion'
import { useContext } from 'react'
import { Navigate, useParams } from 'react-router-dom'
import styled, { useTheme } from 'styled-components'

import { DataContext } from '../DataContext'
import useMedia from '../hooks/useMedia'

function TechnologyItem() {
  const data = useContext(DataContext)

  const tech = useParams().tech?.replace(/-/g, ' ')
  const techData = data.technology.find(d => d.name.toLowerCase() === tech)

  const theme: any = useTheme()
  const isLgUp = useMedia(theme.LG)
  const techImgSrc = isLgUp
    ? techData?.images.portrait
    : techData?.images.landscape

  if (!techData) return <Navigate to=".." />

  return (
    <>
      <Image src={'.' + techImgSrc} alt={techData.name} />
      <Article>
        <Sub>The terminology...</Sub>
        <Heading>{techData.name}</Heading>
        <Text>{techData.description}</Text>
      </Article>
    </>
  )
}

const Image = styled(motion.img).attrs({
  variants: {
    hidden: { opacity: 0 },
    show: { opacity: 1, transition: { duration: 0.5 } },
    exit: { opacity: 0, transition: { duration: 0.5 } },
  },
})`
  width: 100%;
  aspect-ratio: 768 / 310;
  order: 2;
  margin: 32px 0 0;

  @media ${p => p.theme.MD} {
    margin-top: 60px;
  }

  @media ${p => p.theme.LG} {
    aspect-ratio: 515 / 527;
    grid-area: img;
    margin-top: 26px;
  }
`

const Article = styled(motion.article).attrs({
  variants: {
    hidden: { opacity: 0, x: -100 },
    show: { opacity: 1, x: 0, transition: { duration: 0.5 } },
    exit: { opacity: 0, x: -100, transition: { duration: 0.3 } },
  },
})`
  padding: 0 24px;
  margin: 24px 0 32px;
  display: flex;
  flex-direction: column;
  align-items: center;
  order: 4;

  @media ${p => p.theme.MD} {
    margin: 44px 0;
    padding: 0;
    width: 28.625rem;
  }

  @media ${p => p.theme.LG} {
    grid-area: article;
    align-items: flex-start;
    justify-self: center;
    margin: 24px min(9vw, 130px) 0 min(5.5vw, 80px);
    min-width: 29.375rem;
  }
`

const Sub = styled.p`
  font-size: 0.875rem;
  font-family: var(--ff-sans);
  font-weight: 400;
  color: var(--color-light);
  letter-spacing: 2.35px;
  text-transform: uppercase;
  padding: 0;
  margin: 0;

  @media ${p => p.theme.MD} {
    font-size: 1rem;
    letter-spacing: 2.7px;
  }
`

const Heading = styled.h1`
  font-size: 1.5rem;
  font-family: var(--ff-serif);
  font-weight: 400;
  color: var(--color-white);
  text-transform: uppercase;
  padding: 0;
  margin: 9px 0 16px;

  @media ${p => p.theme.MD} {
    font-size: 2.5rem;
    margin-top: 16px;
  }

  @media ${p => p.theme.LG} {
    font-size: 3.5rem;
    margin: 11px 0 17px;
  }
`

const Text = styled.p`
  font-size: 0.9375rem;
  font-family: var(--ff-main);
  font-weight: 400;
  color: var(--color-light);
  padding: 0;
  margin: 0;
  text-align: center;
  line-height: 167%;

  @media ${p => p.theme.MD} {
    font-size: 1rem;
    line-height: 175%;
  }

  @media ${p => p.theme.LG} {
    text-align: left;
    font-size: 1.125rem;
    line-height: 178%;
    padding-right: 1.625rem;
  }
`

export default TechnologyItem
