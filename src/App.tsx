import { useContext } from 'react'
import { Routes, Route, Navigate, useLocation } from 'react-router-dom'
import styled from 'styled-components'
import { AnimatePresence } from 'framer-motion'

import { DataContext } from './DataContext'
import LogoLink from './components/LogoLink'
import Navigation from './components/Navigation'
import Home from './routes/Home'
import Crew from './routes/Crew'
import Destination from './routes/Destination'
import NotFound from './routes/NotFound'
import Technology from './routes/Technology'
import DestinationItem from './routes/DestinationItem'
import CrewItem from './routes/CrewItem'
import TechnologyItem from './routes/TechnologyItem'

function App() {
  const data = useContext(DataContext)
  const location = useLocation()
  const currentPage = location.pathname.split('/')[1]

  return (
    <>
      <Header>
        <LogoLink />
        <Navigation />
      </Header>
      <Main>
        <AnimatePresence exitBeforeEnter>
          <Routes location={location} key={currentPage}>
            <Route path="/" element={<Home />} />
            <Route path="destination" element={<Destination />}>
              <Route path=":dest" element={<DestinationItem />} />
              <Route
                index
                element={
                  <Navigate
                    to={data.destinations[0].name.toLowerCase()}
                    replace
                  />
                }
              />
            </Route>
            <Route path="crew" element={<Crew />}>
              <Route path=":role" element={<CrewItem />} />
              <Route
                index
                element={
                  <Navigate
                    to={data.crew[0].role.toLowerCase().replace(/ /g, '-')}
                    replace
                  />
                }
              />
            </Route>
            <Route path="technology" element={<Technology />}>
              <Route path=":tech" element={<TechnologyItem />} />
              <Route
                index
                element={
                  <Navigate
                    to={data.technology[0].name
                      .toLowerCase()
                      .replace(/ /g, '-')}
                    replace
                  />
                }
              />
            </Route>
            <Route path="*" element={<NotFound />} />
          </Routes>
        </AnimatePresence>
      </Main>
    </>
  )
}

const Header = styled.header`
  padding: 24px;
  display: flex;
  align-items: center;
  justify-content: space-between;

  @media ${p => p.theme.MD} {
    padding: 0;
  }

  @media ${p => p.theme.LG} {
    margin-top: 40px;
  }
`

const Main = styled.main`
  flex: 1;
  display: flex;
  flex-direction: column;
`

export default App
