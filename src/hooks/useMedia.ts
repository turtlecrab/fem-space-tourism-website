import { useEffect, useState } from 'react'

function useMedia(query: string) {
  const [isMedia, setIsMedia] = useState(window.matchMedia(query).matches)

  useEffect(() => {
    function handleResize() {
      setIsMedia(window.matchMedia(query).matches)
    }
    window.addEventListener('resize', handleResize)
    return () => window.removeEventListener('resize', handleResize)
  }, [query])

  return isMedia
}

export default useMedia
