import { createContext } from 'react'

import data from './data.json'
type Data = typeof data

export const DataContext = createContext<Data>({} as Data)
